package input;

import instance.Instance;
import instance.InstanceEj1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class InputEj1 implements Input {

	private String inputFile;
	private LinkedList<Instance> instances;

	private static final String EOF = "#";

	public InputEj1(String inputFile) {
		this.inputFile = inputFile;

	}

	// nos guardamos todas las instancias del archivo de entrada para
	// procesarlos de a uno.

	@Override
	public void readFile() throws Exception {

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				new File(inputFile)));

		instances = new LinkedList<Instance>();

		while (bufferedReader.ready()) {

			String line = bufferedReader.readLine();

			if (!line.startsWith(EOF)) {
				Instance instance = readInstance(line);
				instances.add(instance);
			}
		}

		bufferedReader.close();
	}

	@Override
	public LinkedList<Instance> getInstances() {

		return instances;
	}

	@Override
	public Instance readInstance(String line)
			throws Exception {

		InstanceEj1 subInstance;

		// delimitador espacio en blanco por defecto
		StringTokenizer st = new StringTokenizer(line);

		float limiteCamion = Float.parseFloat(st.nextToken().trim());
		int cantidadPaquetes = Integer.parseInt(st.nextToken().trim());

		float[] paquetes = new float[cantidadPaquetes];
		for (int i = 0; i < cantidadPaquetes && st.hasMoreTokens(); i++) {
			paquetes[i] = Float.parseFloat(st.nextToken().trim());
		}

		subInstance = new InstanceEj1(limiteCamion, cantidadPaquetes, paquetes);
		return subInstance;

	}
}
