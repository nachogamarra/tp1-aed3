package input;

import java.io.BufferedReader;
import java.util.List;
import instance.Instance;

public interface Input {

	//Lee el archivo.
	public void readFile() throws Exception;
	//Devuelve una lista de todas las instacias del archivo.
	public List<Instance> getInstances();
	//Lee una intancia del archivo.
	public Instance readInstance(String line) throws Exception;

}
