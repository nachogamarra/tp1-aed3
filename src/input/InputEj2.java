package input;

import instance.Instance;
import instance.InstanceEj2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.StringTokenizer;

import problem.problem2.Curso;

public class InputEj2 implements Input {

	private String inputFile;
	private LinkedList<Instance> instances;

	private static final String EOF = "#";

	public InputEj2(String inputFile) {
		this.inputFile = inputFile;

	}

	// nos guardamos todas las instancias del archivo de entrada para
	// procesarlos de a uno.

	@Override
	public void readFile() throws Exception {

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				new File(inputFile)));

		instances = new LinkedList<Instance>();

		while (bufferedReader.ready()) {

			String line = bufferedReader.readLine();

			if (!line.startsWith(EOF)) {
				Instance instance = readInstance(line);
				instances.add(instance);
			}
		}

		bufferedReader.close();
	}

	@Override
	public LinkedList<Instance> getInstances() {

		return instances;
	}

	@Override
	public Instance readInstance(String line) throws Exception {

		InstanceEj2 subInstance;

		// delimitador espacio en blanco por defecto
		StringTokenizer st = new StringTokenizer(line);

		int cantidadCursos = Integer.parseInt(st.nextToken().trim());
		Curso[] cursos = new Curso[cantidadCursos];

		for (int i = 0; i < cantidadCursos && st.hasMoreTokens(); i++) {

			// Armo un curso.
			int nroCurso = i + 1;
			int inicioCurso = Integer.parseInt(st.nextToken().trim());
			int finCurso = Integer.parseInt(st.nextToken().trim());

			Curso curso = new Curso(nroCurso, inicioCurso, finCurso);
			cursos[i] = curso;

		}

		subInstance = new InstanceEj2(cantidadCursos, cursos);
		return subInstance;

	}
}
