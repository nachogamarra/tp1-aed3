package test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Test {

	// Aca se carga el path de la carpeta resources del proyecto.
	public static String resourcesPath;

	// Para cargar un archivo de prueba desde el archivo .properties.
	public static String testFile;

	// Archivos standard de entrada.
	public static String testFileEj1in;
	public static String testFileEj2in;
	public static String testFileEj3in;

	// Archivos standard de salida.
	public static String testFileEj1out;
	public static String testFileEj2out;
	public static String testFileEj3out;
	
	//Tests
	//Problema 1
	public static String test_ej1_caso_arbit;
	public static String test_ej1_caso_arbit_out;

	public static String test_ej1_limite;
	public static String test_ej1_limite_out;
	
	public static String test_ej1_1camion;
	public static String test_ej1_1camion_out;

	public static String test_ej1_casos_aleat;
	public static String test_ej1_casos_aleat_out;
	public static String mediciones_casos_aleat;
	
	public static String test_ej1_casos_peores;
	public static String test_ej1_casos_peores_out;
	public static String mediciones_casos_peores;
	
	public static String test_ej1_casos_mejores;
	public static String test_ej1_casos_mejores_out;
	public static String mediciones_casos_mejores;
	
	//Problema 2
	public static String test_ej2_caso_arbit;
	public static String test_ej2_caso_arbit_out;
	
	public static String test_ej2_mas_de_una_sol;
	public static String test_ej2_mas_de_una_sol_out;
	
	public static String test_ej2_unica;
	public static String test_ej2_unica_out;
	
	public static String test_ej2_casos_aleat;
	public static String test_ej2_casos_aleat_out;
	public static String ej2_mediciones_casos_aleat;
	
	
	public Properties properties;

	public Test() throws FileNotFoundException, IOException {

		properties = new Properties();
		properties.load(new FileInputStream("global.properties"));

		System.out.println("user.dir: " + System.getProperty("user.dir"));

		// Obtenemos el nombre del directorio donde est�n los archivos.
		resourcesPath = System.getProperty("user.dir")
				+ properties.getProperty("RESOURCES_RELATIVE_PATH");

		// Para evitar problemas con los par�ntesis UNIX o Windows. "/" o "\".
		// Unificamos.
		String fileSeparator = System.getProperty("file.separator");
		System.out.println("System file separator: " + fileSeparator);

		resourcesPath = resourcesPath.replace("/", fileSeparator);
		resourcesPath = resourcesPath.replace("\\", fileSeparator);
		System.out.println("resourcesPath: " + resourcesPath);

		loadEj1Tests();
		loadEj2Tests();

		// Instancias generadas para probar el ejercicio 3.
		loadEj3Tests();

	}

	private void loadEj3Tests() {
		// TODO Auto-generated method stub
	}

	private void loadEj2Tests() {

		// Obtenemos el nombre del archivo para testear.

		//Test: Caso Arbitrario
		test_ej2_caso_arbit = resourcesPath + properties.getProperty("TEST_FILE_EJ2_IN_CASO_ARBITRARIO");
		System.out.println("testFile: " + test_ej2_caso_arbit);

		test_ej2_caso_arbit_out = resourcesPath + properties.getProperty("TEST_FILE_EJ2_OUT_CASO_ARBITRARIO");
		System.out.println("testFile: " + test_ej2_caso_arbit_out);

		//Test: Mas de una solucion

		test_ej2_mas_de_una_sol = resourcesPath + properties.getProperty("TEST_FILE_EJ2_IN_MAS_DE_UNA");
		System.out.println("testFile: " + 
				test_ej2_mas_de_una_sol);

		test_ej2_mas_de_una_sol_out = resourcesPath + properties.getProperty("TEST_FILE_EJ2_OUT_MAS_DE_UNA");
		System.out.println("testFile: " + 
				test_ej2_mas_de_una_sol);
		
		
		//Test: Instancia de un unico curso

		test_ej2_unica = resourcesPath + properties.getProperty("TEST_FILE_EJ2_IN_UNICA");
		System.out.println("testFile: " + test_ej2_unica);

		test_ej2_unica_out = resourcesPath + properties.getProperty("TEST_FILE_EJ2_OUT_UNICA");
		System.out.println("testFile: " + test_ej2_unica_out);

		
		//Test: Casos Aleatorios
		ej2_mediciones_casos_aleat = resourcesPath + properties.getProperty("TEST_FILE_EJ2_OUT_MEDICIONES_CASOS_ALEAT");
		System.out.println("testFile: " + ej2_mediciones_casos_aleat);
		
		test_ej2_casos_aleat = resourcesPath + properties.getProperty("TEST_FILE_EJ2_IN_CASOS_ALEATORIOS");
		System.out.println("testFile: " + test_ej2_casos_aleat);
		
		test_ej2_casos_aleat_out = resourcesPath + properties.getProperty("TEST_FILE_EJ2_OUT_CASOS_ALEATORIOS");
		System.out.println("testFile: " + test_ej2_casos_aleat_out);

	}

	private void loadEj1Tests() {

		// Obtenemos el nombre del archivo para testear.

		//Test: Caso Arbitrario
		test_ej1_caso_arbit = resourcesPath + properties.getProperty("TEST_FILE_EJ1_IN_CASO_ARBITRARIO");
		System.out.println("testFile: " + test_ej1_caso_arbit);

		test_ej1_caso_arbit_out = resourcesPath + properties.getProperty("TEST_FILE_EJ1_OUT_CASO_ARBITRARIO");
		System.out.println("testFile: " + test_ej1_caso_arbit_out);

		//Test: Los paquetes alcanzan el limite
		test_ej1_limite = resourcesPath + properties.getProperty("TEST_FILE_EJ1_IN_LIMITE");
		System.out.println("testFile: " + test_ej1_limite);

		test_ej1_limite_out = resourcesPath + properties.getProperty("TEST_FILE_EJ1_OUT_LIMITE");
		System.out.println("testFile: " + test_ej1_limite_out);
		
		//Test: Los paquetes que pueden entrar en un solo camion
		test_ej1_1camion = resourcesPath + properties.getProperty("TEST_FILE_EJ1_IN_1CAMION");
		System.out.println("testFile: " + test_ej1_1camion);

		test_ej1_1camion_out = resourcesPath + properties.getProperty("TEST_FILE_EJ1_OUT_1CAMION");
		System.out.println("testFile: " + test_ej1_1camion_out);

		//Test: Casos Aleatorios
		mediciones_casos_aleat = resourcesPath + properties.getProperty("TEST_FILE_EJ1_OUT_MEDICIONES_CASOS_ALEAT");
		System.out.println("testFile: " + mediciones_casos_aleat);
		
		test_ej1_casos_aleat = resourcesPath + properties.getProperty("TEST_FILE_EJ1_IN_CASOS_ALEATORIOS");
		System.out.println("testFile: " + test_ej1_casos_aleat);
		
		test_ej1_casos_aleat_out = resourcesPath + properties.getProperty("TEST_FILE_EJ1_OUT_CASOS_ALEATORIOS");
		System.out.println("testFile: " + test_ej1_casos_aleat_out);

		//Test: Casos Peores
		mediciones_casos_peores = resourcesPath + properties.getProperty("TEST_FILE_EJ1_OUT_MEDICIONES_CASOS_PEORES");
		System.out.println("testFile: " + mediciones_casos_peores);
		
		test_ej1_casos_peores = resourcesPath + properties.getProperty("TEST_FILE_EJ1_IN_CASOS_PEORES");
		System.out.println("testFile: " + test_ej1_casos_peores);

		test_ej1_casos_peores_out = resourcesPath + properties.getProperty("TEST_FILE_EJ1_OUT_CASOS_PEORES");
		System.out.println("testFile: " + test_ej1_casos_peores_out);
		
		//Test: Casos Mejores
		mediciones_casos_mejores = resourcesPath + properties.getProperty("TEST_FILE_EJ1_OUT_MEDICIONES_CASOS_MEJORES");
		System.out.println("testFile: " + mediciones_casos_mejores);
		
		test_ej1_casos_mejores = resourcesPath + properties.getProperty("TEST_FILE_EJ1_IN_CASOS_MEJORES");
		System.out.println("testFile: " + test_ej1_casos_mejores);

		test_ej1_casos_mejores_out = resourcesPath + properties.getProperty("TEST_FILE_EJ1_OUT_CASOS_MEJORES");
		System.out.println("testFile: " + test_ej1_casos_mejores_out);
		
	}

}
