package test;

import input.InputEj1;
import instance.Instance;
import instance.InstanceEj1;
import instance.InstanceEj3;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Random;

import problem.problem1.Camion;
import problem.problem1.Problem1;

import org.junit.experimental.theories.suppliers.TestedOn;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import output.OutputEj1;

public class TestEj1 extends test.Test {

	public TestEj1() throws FileNotFoundException, IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	void runTest(String inputFile, String outputFile, String medicionesFile)
			throws Exception {

		InputEj1 input = new InputEj1(inputFile);
		input.readFile();

		// Obtengo las instancias del archivo.
		List<Instance> instances = input.getInstances();
		List<List<Camion>> solutions = new ArrayList<List<Camion>>();
		BufferedWriter bw = null;

		if (medicionesFile != null) {
			bw = new BufferedWriter(new PrintWriter(medicionesFile));
		}
		// Ejecutamos el algoritmo por cada una de las instancias.
		for (int i = 0; i < instances.size(); i++) {
			InstanceEj1 instance = (InstanceEj1) instances.get(i);
			Problem1 problem = new Problem1(instance.getLimiteCamion(),
					instance.getCantidadPaquetes(), instance.getPaquetes());
			problem.colocarPaquetes(instance.getLimiteCamion(),
					instance.getCantidadPaquetes(), instance.getPaquetes());

			List<Camion> result = problem.getCamionesOrdenadosXLlegada();

			if (medicionesFile != null) {
				printMedicion(bw, instance.getCantidadPaquetes(),
						problem.getContadorOperaciones());
			}

			solutions.add(result);

		}

		if (medicionesFile != null) {
			bw.close();
			medicionesFile = null;
		}

		OutputEj1 output = new OutputEj1(solutions);
		output.writeFile(outputFile);

	}

	private void printMedicion(BufferedWriter bw, int inputSize,
			int contadorOperaciones) throws IOException {

		// bw.write(Integer.toString(inputSize));
		// bw.write(" ");
		bw.write(Integer.toString(contadorOperaciones));
		bw.write(" ");
		bw.newLine();
	}

	//@Test
	public void test_ej1_caso_arbit() throws Exception {
		runTest(test.Test.test_ej1_caso_arbit,
				test.Test.test_ej1_caso_arbit_out, null);
	}

	//@Test
	public void test_ej1_limite() throws Exception {
		runTest(test.Test.test_ej1_limite, test.Test.test_ej1_limite_out, null);
	}

	//@Test
	public void test_ej1_1camion() throws Exception {
		runTest(test.Test.test_ej1_1camion, test.Test.test_ej1_1camion_out,
				null);
	}

	//@Test
	public void test_generador_casos_aleatorios() throws Exception {
		InstanceEj1 instancia_actual;
		int n = 100; // cantidad de instancias a generar
		List<InstanceEj1> instancias = new ArrayList<InstanceEj1>(n);
		int limite_aux = 0;
		BufferedWriter bufferedWriter = new BufferedWriter(new PrintWriter(
				test.Test.test_ej1_casos_aleat));
		Random r = new Random(); // Para generar instancias aleatorias
		for (int i = 1; i <= n; i++) { // las instancias aumentan segun i
			float[] paquetes_actuales = new float[i];
			while (limite_aux == 0) { // para que el limite no sea 0
				limite_aux = Math.abs(r.nextInt(100)); // El limite es aleatorio
			}
			for (int j = 0; j < i; j++) {
				while (paquetes_actuales[j] == 0) { // para que el paquete no
													// sea 0
					paquetes_actuales[j] = r.nextInt(limite_aux); // El peso de
																	// los
																	// paquetes
																	// es
																	// aleatorio
				}
			}
			instancia_actual = new InstanceEj1(limite_aux, i, paquetes_actuales); // Creo
																					// la
																					// instancia
																					// con
																					// los
																					// valores
																					// creados
																					// anteriormente
			instancias.add(instancia_actual); // Añado la instancia de tamaño i
												// a la lista de instancias
			printInst(bufferedWriter, instancias.get(i - 1)); // Imprimo la
																// instancia en
																// el archivo
			limite_aux = 0; // para que me genere un nuevo limite en la proxima
							// instancia
		}
		bufferedWriter.write("#");
		bufferedWriter.close();
	}

	//@Test
	public void test_generador_casos_peores() throws Exception {
		InstanceEj1 instancia_actual;
		int n = 100; // cantidad de instancias a generar
		List<InstanceEj1> instancias = new ArrayList<InstanceEj1>(n);
		int limite_aux = 0;
		BufferedWriter bufferedWriter = new BufferedWriter(new PrintWriter(
				test.Test.test_ej1_casos_peores));
		Random r = new Random();
		for (int i = 1; i <= n; i++) { // las instancias aumentan segun i
			float[] paquetes_actuales = new float[i];
			while (limite_aux == 0) { // para que el limite no sea 0
				limite_aux = Math.abs(r.nextInt(100));
			}
			for (int j = 0; j < i; j++) {
				paquetes_actuales[j] = limite_aux; // para que sea uno de
													// los peores casos los
													// paquetes deben
													// alcanzar el limite
													// del camion
			}
			instancia_actual = new InstanceEj1(limite_aux, i, paquetes_actuales);
			instancias.add(instancia_actual);
			printInst(bufferedWriter, instancias.get(i - 1));
			limite_aux = 0; // para que me genere un nuevo limite en la proxima
							// instancia
		}
		bufferedWriter.write("#");
		bufferedWriter.close();
	}
	
	//@Test
	public void test_generador_casos_mejores() throws Exception {
		InstanceEj1 instancia_actual;
		int n = 100; // cantidad de instancias a generar
		List<InstanceEj1> instancias = new ArrayList<InstanceEj1>(n);
		int limite_aux = 0;
		BufferedWriter bufferedWriter = new BufferedWriter(new PrintWriter(
				test.Test.test_ej1_casos_mejores));
		Random r = new Random();
		for (int i = 1; i <= n; i++) { // las instancias aumentan segun i
			float[] paquetes_actuales = new float[i];
			while (limite_aux == 0) { // para que el limite no sea 0
				limite_aux = Math.abs(r.nextInt(100));
			}
			for (int j = 0; j < i; j++) {
				paquetes_actuales[j] = limite_aux/i; // para que sea uno de
													// los mejores casos los
													// paquetes deben
													// ser a lo sumo
													// limite_aux/i
			}
			instancia_actual = new InstanceEj1(limite_aux, i, paquetes_actuales);
			instancias.add(instancia_actual);
			printInst(bufferedWriter, instancias.get(i - 1));
			limite_aux = 0; // para que me genere un nuevo limite en la proxima
							// instancia
		}
		bufferedWriter.write("#");
		bufferedWriter.close();
	}	

	// Auxiliar para imprimir en el archivo
	private void printInst(BufferedWriter bufferedWriter, InstanceEj1 instancia)
			throws Exception {

		float k = instancia.getLimiteCamion();
		bufferedWriter.write(String.valueOf(k));
		bufferedWriter.write(" ");
		bufferedWriter.write(String.valueOf(instancia.getCantidadPaquetes()));

		for (int i = 0; i < instancia.getCantidadPaquetes(); i++) {
			bufferedWriter.write(" ");
			bufferedWriter.write(Float.toString((instancia.getPaquetes())[i]));

		}

		bufferedWriter.flush();
		bufferedWriter.newLine();
	}

	//@Test
	public void test_ej1_casos_aleat() throws Exception {
		runTest(test.Test.test_ej1_casos_aleat,
				test.Test.test_ej1_casos_aleat_out,
				test.Test.mediciones_casos_aleat);
	}

	//@Test
	public void test_ej1_casos_peores() throws Exception {
		runTest(test.Test.test_ej1_casos_peores,
				test.Test.test_ej1_casos_peores_out, mediciones_casos_peores);
	}
	
	//@Test
	public void test_ej1_casos_mejores() throws Exception {
		runTest(test.Test.test_ej1_casos_mejores,
				test.Test.test_ej1_casos_mejores_out, mediciones_casos_mejores);
	}	
}
