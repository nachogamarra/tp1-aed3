package test;

import input.InputEj2;
import instance.Instance;
import instance.InstanceEj1;
import instance.InstanceEj2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Random;

import org.junit.experimental.theories.suppliers.TestedOn;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import output.OutputEj2;
import problem.problem2.Curso;
import problem.problem2.Problem2;

public class TestEj2 extends test.Test {

	public TestEj2() throws FileNotFoundException, IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	void runTest(String inputFile, String outputFile, String medicionesFile)
			throws Exception {

		InputEj2 input = new InputEj2(inputFile);
		input.readFile();

		// Obtengo las instancias del archivo.
		List<Instance> instances = input.getInstances();
		List<List<Integer>> solutions = new ArrayList<List<Integer>>();
		BufferedWriter bw = null;

		if (medicionesFile != null) {
			bw = new BufferedWriter(new PrintWriter(medicionesFile));
		}
		// Ejecutamos el algoritmo por cada una de las instancias.
		for (int i = 0; i < instances.size(); i++) {
			InstanceEj2 instance = (InstanceEj2) instances.get(i);
			ArrayList<Integer> solucion = new ArrayList<Integer>();
			ArrayList<Curso> cursos_vec = new ArrayList<Curso>();
			for (int j = 0; j < instance.getCantidadCursos(); j++)
				cursos_vec.add(instance.getCursos()[j]);
			Problem2 problem = new Problem2(cursos_vec,
					instance.getCantidadCursos(), solucion);

			ArrayList<Integer> result = problem.getSolucion();

			if (medicionesFile != null) {
				printMedicion(bw, instance.getCantidadCursos(),
						problem.getContadorOperaciones());
			}

			solutions.add(result);

		}

		if (medicionesFile != null) {
			bw.close();
			medicionesFile = null;
		}

		OutputEj2 output = new OutputEj2(solutions);
		output.writeFile(outputFile);

	}

	private void printMedicion(BufferedWriter bw, int inputSize,
			int contadorOperaciones) throws IOException {

		// bw.write(Integer.toString(inputSize));
		// bw.write(" ");
		bw.write(Integer.toString(contadorOperaciones));
		bw.write(" ");
		bw.newLine();
	}

	// @Test
	public void test_ej2_caso_arbit() throws Exception {
		runTest(test.Test.test_ej2_caso_arbit,
				test.Test.test_ej2_caso_arbit_out, null);
	}

	//@Test
	public void test_ej2_mas_de_una_sol() throws Exception {
		runTest(test.Test.test_ej2_mas_de_una_sol,
				test.Test.test_ej2_mas_de_una_sol_out, null);
	}	
	
	@Test
	public void test_ej2_unica() throws Exception {
		runTest(test.Test.test_ej2_unica,
				test.Test.test_ej2_unica_out, null);
	}	
	
	//@Test
	public void test_generador_casos_aleatorios() throws Exception {
		InstanceEj2 instancia_actual;
		int n = 100; // cantidad de instancias a generar
		List<InstanceEj2> instancias = new ArrayList<InstanceEj2>(n);

		BufferedWriter bufferedWriter = new BufferedWriter(new PrintWriter(
				test.Test.test_ej2_casos_aleat));
		int limite_aux = 0;
		Random r = new Random(); // Para generar instancias aleatorias
		for (int i = 1; i <= n; i++) { // las instancias aumentan segun i
			Curso[] cursos_actuales = new Curso[i];
			while (limite_aux == 0) { // para que genere un limite nuevo para
										// cada instancia y este no sea 0
				limite_aux = Math.abs(r.nextInt(100)); // El limite es aleatorio
			}
			for (int j = 0; j < i; j++) {
				int inicio_actual;
				int fin_actual = 0;

				inicio_actual = r.nextInt(limite_aux);
				fin_actual = r.nextInt(limite_aux) + inicio_actual + 1; // para que
																	// obtenga
																	// un random
																	// entre
																	// inicio_actual+1
																	// y
																	// limite_actual+inicio_actual+1

				cursos_actuales[j] = new Curso(j + 1, inicio_actual, fin_actual);
			}
			instancia_actual = new InstanceEj2(i, cursos_actuales); // Creo
																	// la
																	// instancia
																	// con
																	// los
																	// valores
																	// creados
																	// anteriormente
			instancias.add(instancia_actual); // Añado la instancia de tamaño i
												// a la lista de instancias
			printInst(bufferedWriter, instancias.get(i - 1)); // Imprimo la
																// instancia en
																// el archivo
			limite_aux = 0; // para que me genere un nuevo limite en la proxima
							// instancia
		}
		bufferedWriter.write("#");
		bufferedWriter.close();
	}
	
	//@Test
	public void test_ej2_casos_aleat() throws Exception {
		runTest(test.Test.test_ej2_casos_aleat,
				test.Test.test_ej2_casos_aleat_out,
				test.Test.ej2_mediciones_casos_aleat);
	}

	// Auxiliar para imprimir en el archivo
	private void printInst(BufferedWriter bufferedWriter, InstanceEj2 instancia)
			throws Exception {

		bufferedWriter.write(Integer.toString((instancia.getCantidadCursos())));

		for (int i = 0; i < instancia.getCantidadCursos(); i++) {
			bufferedWriter.write(" ");
			bufferedWriter.write(Integer.toString((instancia.getCursos())[i]
					.getInicio()));
			bufferedWriter.write(" ");
			bufferedWriter.write(Integer.toString((instancia.getCursos())[i]
					.getFin()));
		}

		bufferedWriter.flush();
		bufferedWriter.newLine();
	}

}