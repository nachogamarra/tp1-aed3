package problem.problem2;

import java.util.Collections;
import java.util.ArrayList;

public class Problem2 {

	private ArrayList<Integer> solucion;

	public ArrayList<Integer> getSolucion() {
		return solucion;
	}

	private int contadorOperaciones;

	public int getContadorOperaciones() {
		return contadorOperaciones;
	}

	public Problem2(ArrayList<Curso> cursos, int cantCursos,
			ArrayList<Integer> solucion) {
		resolver(cursos, cantCursos, solucion);
	}

	public void resolver(ArrayList<Curso> cursos, int cantCursos,
			ArrayList<Integer> solucion) {

		contadorOperaciones = 0;
		this.solucion = solucion;
		CursoComparator comparator = new CursoComparator();
		contadorOperaciones = +2;

		// Ordeno los intervalos segun finalizacion
		Collections.sort(cursos, comparator);
		contadorOperaciones += cursos.size() * (int) Math.log(cursos.size());
		// Siempre el primer intervalo es solucion
		int x = 0;
		solucion.add(cursos.get(0).getNroCurso());
		contadorOperaciones += 3;

		/* Recorro los intervalos ordenados */
		for (int i = 1; i < cantCursos; i++) {
			// Si no se solapan, agregar el numero intervalo evaluado a la
			// solucion

			contadorOperaciones += 12;
			if (!cursos.get(x).seSolapan(cursos.get(i))) {
				solucion.add(cursos.get(i).getNroCurso());
				// Ahora evaluar contra el ultimo intervalo agregado
				x = i;
				contadorOperaciones += 4;
			}
		}
	}
}
