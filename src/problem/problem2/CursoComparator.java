package problem.problem2;

import java.util.Comparator;

public class CursoComparator implements Comparator<Curso> {
	public int compare(Curso arg0, Curso arg1) {
		int resultado;
		if (arg0.getFin() < arg1.getFin())
			resultado = -1;
		else if (arg0.getFin() == arg1.getFin())
			resultado = 0;
		else
			resultado = 1;
		return resultado;
	}
}
