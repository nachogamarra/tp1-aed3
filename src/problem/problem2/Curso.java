package problem.problem2;

public class Curso {

	private int nroCurso;
	private int inicio;
	private int fin;

	public Curso(int nroCurso,int inicio, int fin) {
		this.nroCurso = nroCurso;
		this.inicio = inicio;
		this.fin = fin;
	}
	
	public int getNroCurso() {
		return nroCurso;
	}
	
	public int getInicio() {
		return inicio;
	}

	public int getFin() {
		return fin;
	}

	public boolean seSolapan(Curso c2) {
		return (this.fin > c2.getInicio());
	}

}
