package problem.problem3;

public class LocatedSensor {

	private Coordinate c;
	private int sensorType;
	private boolean coverImportantCellHorizontally;
	private boolean coverImportantCellVertically;

	public LocatedSensor(Coordinate c, int sensorType, boolean coverImportantCellHorizontally, boolean coverImportantCellVertically) {

		this.c = c;
		this.sensorType = sensorType;
		this.coverImportantCellHorizontally = coverImportantCellHorizontally;
		this.coverImportantCellVertically = coverImportantCellVertically;
		
	}

	public boolean isCoverImportantCellHorizontally() {
		return coverImportantCellHorizontally;
	}

	public void setCoverImportantCellHorizontally(
			boolean coverImportantCellHorizontally) {
		this.coverImportantCellHorizontally = coverImportantCellHorizontally;
	}

	public boolean isCoverImportantCellVertically() {
		return coverImportantCellVertically;
	}

	public void setCoverImportantCellVertically(boolean coverImportantCellVertically) {
		this.coverImportantCellVertically = coverImportantCellVertically;
	}

	public Coordinate getCoordinate() {
		return c;
	}

	public void setCoordinate(Coordinate c) {
		this.c = c;
	}

	public int getSensorType() {
		return sensorType;
	}

	public void setSensorType(int sensorType) {
		this.sensorType = sensorType;
	}

}
