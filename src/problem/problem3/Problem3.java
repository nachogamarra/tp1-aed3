package problem.problem3;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Problem3 {

	// Tipo de sensores
	private static final int BI_SENSOR_HORIZONTAL = 0;
	private static final int BI_SENSOR_VERTICAL = 1;
	private static final int CUAT_SENSOR = 2;

	// Costo de los sensores.
	private static final int BI_SENSOR_COST = 4000;
	private static final int CUAT_SENSOR_COST = 6000;

	// Guardamos m�nimo costo y ubicaci�n de los sensores.
	private int minCostAccumulator;
	private Stack<LocatedSensor> sensorsSetup = null;

	// Cantidad total de celdas a cubrir por los sensores en el piso.
	private int totalCellsToCover;

	// Mapa del piso.
	private Floor floorMap = null;

	// Mapa del piso a ser manipulado.
	private TrackingFloor trackingFloorMap;

	// Para saber la direccion del sensor necesitado.
	private boolean horizontalSensorNeeded = false;
	private boolean verticalSensorNeeded = false;
	private boolean horizontalSensorNeededTemp;
	private boolean verticalSensorNeededTemp;
	private int cellsCovered;

	public static void main(String[] args) {

		int[][] floor = { { 0, 0, 0, 1, 1, 2, 0, 0 },
				{ 0, 1, 1, 1, 1, 1, 1, 1 }, { 0, 1, 2, 1, 0, 1, 1, 0 },
				{ 0, 1, 2, 1, 0, 1, 1, 0 }, { 1, 1, 1, 1, 1, 1, 1, 1 },
				{ 0, 0, 0, 1, 0, 0, 0, 0 }, { 1, 0, 1, 1, 1, 1, 1, 0 },
				{ 1, 1, 1, 1, 2, 1, 1, 0 }

		};

		try {
			Problem3 p = new Problem3(8, 8, floor);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Problem3(int n, int m, int[][] floor)
			throws CloneNotSupportedException {

		floorMap = new Floor(n, m, floor);
		searchSolution();

	}

	private void searchSolution() throws CloneNotSupportedException {
		resetTrackingFloor();
		sensorsSetup = new Stack<LocatedSensor>();
		minCostAccumulator = 0;

		// Aca se hace el backtracking.
		coverFloor();
	}

	// Celdas libres a cubrir por los sensores.
	private void resetCellsToCover(Floor floorMap) {
		totalCellsToCover = floorMap.getFreeCells()
				+ floorMap.getImportantCells();
	}

	// Se resetea el tracking floor.
	private void resetTrackingFloor() throws CloneNotSupportedException {
		trackingFloorMap = new TrackingFloor(floorMap);
		resetCellsToCover(floorMap);
	}

	private void coverFloor() {

		coverImportantCells();
		coverRestOfFloor(0, 0);

	}

	// Cubre las celdas mas importantes de la forma mas �ptima posible.
	// Ubicando sensores que optimizen la cantidad de celdas cubiertas.
	private void coverImportantCells() {

		for (int i = 0; i < floorMap.getImportantCells(); i++) {
			Coordinate c = floorMap.getImportantCellsList().get(i);
			coverImportantCell(c);
		}
	}

	// Busca la forma mas �ptima de cubrir una celda importante.
	// PRIMERA optimizacion-poda.
	private void coverImportantCell(Coordinate c) {

		// Ubicacion vertical del sensor, con respecto a la celda importante.

		if (!isVerticallyCovered(c)) {
			Coordinate verticalCoordinate = findOptimalVerticalCover(c);

			if (cellsCovered > 0) {
				int verticalSensorType = -1;

				if (horizontalSensorNeeded && verticalSensorNeeded) {
					verticalSensorType = CUAT_SENSOR;

				} else if (verticalSensorNeeded) {
					verticalSensorType = BI_SENSOR_VERTICAL;
				}

				if (!trackingFloorMap.isCovered(verticalCoordinate.getX(),
						verticalCoordinate.getY())) {
					LocatedSensor locatedSensor = new LocatedSensor(
							verticalCoordinate, verticalSensorType, false, true);
					sensorsSetup.push(locatedSensor);
					coverCells(verticalCoordinate, verticalSensorType);
				}
			} else {
				backtrackStepImportantCells(c);
			}
		}

		if (!isHorizontallyCovered(c)) {
			// Ubicacion horizontal del sensor, con respecto a la celda
			// importante.
			Coordinate horizontalCoordinate = findOptimalHorizontalCover(c);

			if (cellsCovered > 0) {
				int horizontalSensorType = -1;

				if (horizontalSensorNeeded && verticalSensorNeeded) {
					horizontalSensorType = CUAT_SENSOR;

				} else if (horizontalSensorNeeded) {
					horizontalSensorType = BI_SENSOR_HORIZONTAL;

				}

				if (!trackingFloorMap.isCovered(horizontalCoordinate.getX(),
						horizontalCoordinate.getY())) {
					LocatedSensor locatedSensor = new LocatedSensor(
							horizontalCoordinate, horizontalSensorType, true,
							false);
					sensorsSetup.push(locatedSensor);
					coverCells(horizontalCoordinate, horizontalSensorType);
				}
			} else {
				backtrackStepImportantCells(c);
			}
		}

	}

	// Retrocede un paso en el �rbol de busqueda, mientras cubre las celdas
	// importantes.
	private void backtrackStepImportantCells(Coordinate c) {

		if (!sensorsSetup.empty()) {
			// Sacamos el sensor y lo usamos para liberar las celdas que cubre.
			LocatedSensor sensor = (LocatedSensor) sensorsSetup.pop();

			// Se liberan las celdas cubiertas por el sensor eliminado.
			uncoverCells(sensor);

			if (sensor.getSensorType() == CUAT_SENSOR) {
				if (sensor.isCoverImportantCellHorizontally()) {
					LocatedSensor locatedSensor = new LocatedSensor(
							sensor.getCoordinate(), BI_SENSOR_HORIZONTAL, true,
							false);
					sensorsSetup.push(locatedSensor);
					coverCells(sensor.getCoordinate(), BI_SENSOR_HORIZONTAL);
				} else if (sensor.isCoverImportantCellVertically()) {
					LocatedSensor locatedSensor = new LocatedSensor(
							sensor.getCoordinate(), BI_SENSOR_VERTICAL, false,
							true);
					sensorsSetup.push(locatedSensor);
					coverCells(sensor.getCoordinate(), BI_SENSOR_VERTICAL);
				}
			} else {

			}

			// Una vez hecho el retroceso, se intenta nuevamente cubrir la celda
			// importante.
			coverImportantCell(c);

		} else {
			noSolution();
		}

	}

	// Verifica si una coordenada es cubierta horizontalmente por alguno de los
	// sensores.
	private boolean isHorizontallyCovered(Coordinate c) {

		boolean result = false;

		for (int i = 0; i < sensorsSetup.size(); i++) {
			LocatedSensor sensor = (LocatedSensor) sensorsSetup.get(i);
			if (sensor.getCoordinate().getX() == c.getX()) {
				if (sensor.getSensorType() == CUAT_SENSOR
						|| sensor.getSensorType() == BI_SENSOR_HORIZONTAL) {
					result = result
							|| existsHorizontalPath(c, sensor.getCoordinate());
				}
			}
		}

		return result;
	}

	// Verifica si una coordenada es cubierta verticalmente por alguno de los
	// sensores.
	private boolean isVerticallyCovered(Coordinate c) {
		boolean result = false;

		for (int i = 0; i < sensorsSetup.size(); i++) {
			LocatedSensor sensor = (LocatedSensor) sensorsSetup.get(i);

			if (sensor.getCoordinate().getY() == c.getY()) {
				if (sensor.getSensorType() == CUAT_SENSOR
						|| sensor.getSensorType() == BI_SENSOR_VERTICAL) {
					result = result
							|| existsVerticalPath(c, sensor.getCoordinate());
				}
			}
		}

		return result;
	}

	// Cubre las celdas dada la ubicacion y tipo del sensor.
	private void coverCells(Coordinate c, int sensorType) {

		if (sensorType == CUAT_SENSOR) {

			for (int i = 0; i < floorMap.getN(); i++) {
				if (trackingFloorMap.getPos(i, c.getY()) != TrackingFloor.WALL_CELL
						&& existsVerticalPath(c, new Coordinate(i, c.getY()))) {
					trackingFloorMap.cover(i, c.getY());
					if (trackingFloorMap.getPos(i, c.getY()) == TrackingFloor.JUST_COVERED_FREE_CELL)
						totalCellsToCover--;
				}
			}

			for (int j = 0; j < floorMap.getM(); j++) {
				if (trackingFloorMap.getPos(c.getX(), j) != TrackingFloor.WALL_CELL
						&& existsHorizontalPath(c, new Coordinate(c.getX(), j))) {
					trackingFloorMap.cover(c.getX(), j);
					if (trackingFloorMap.getPos(c.getX(), j) == TrackingFloor.JUST_COVERED_FREE_CELL)
						totalCellsToCover--;
				}
			}

			// Descubrimos la coordenada a ubicar el sensor,
			// ya que cubrimos dos veces por recorrer las dos dimensiones.
			trackingFloorMap.uncover(c.getX(), c.getY());
			minCostAccumulator += CUAT_SENSOR_COST;

		} else if (sensorType == BI_SENSOR_VERTICAL) {
			for (int i = 0; i < floorMap.getN(); i++) {
				if (trackingFloorMap.getPos(i, c.getY()) != TrackingFloor.WALL_CELL
						&& existsVerticalPath(c, new Coordinate(i, c.getY()))) {
					trackingFloorMap.cover(i, c.getY());
					if (trackingFloorMap.getPos(i, c.getY()) == TrackingFloor.JUST_COVERED_FREE_CELL)
						totalCellsToCover--;
				}
			}
			minCostAccumulator += BI_SENSOR_COST;

		} else if (sensorType == BI_SENSOR_HORIZONTAL) {
			for (int j = 0; j < floorMap.getM(); j++) {
				if (trackingFloorMap.getPos(c.getX(), j) != TrackingFloor.WALL_CELL
						&& existsHorizontalPath(c, new Coordinate(c.getX(), j))) {
					trackingFloorMap.cover(c.getX(), j);
					if (trackingFloorMap.getPos(c.getX(), j) == TrackingFloor.JUST_COVERED_FREE_CELL)
						totalCellsToCover--;
				}
			}
			minCostAccumulator += BI_SENSOR_COST;
		}
	}

	// Busca la forma mas optima de cubrir horizontalmente una celda importante.
	private Coordinate findOptimalHorizontalCover(Coordinate c) {

		cellsCovered = 0;
		Coordinate sensorCoordinate = new Coordinate();

		for (int i = 0; i < floorMap.getM(); i++) {
			if (c.getY() != i) {
				Coordinate actualCoordinate = new Coordinate(c.getX(), i);
				if (floorMap.getPos(c.getX(), i) != Floor.WALL
						&& existsHorizontalPath(c, actualCoordinate)) {
					int cellsToCoverCardinal = getCellsToCoverCardinal(
							c.getX(), i);
					if (cellsToCoverCardinal > cellsCovered) {
						cellsCovered = cellsToCoverCardinal;
						sensorCoordinate.setX(c.getX());
						sensorCoordinate.setY(i);
						horizontalSensorNeeded = true;
						verticalSensorNeeded = verticalSensorNeededTemp;

					}
				}
			}
		}

		return sensorCoordinate;

	}

	// No hay paredes bloqueando el camino horizontal entre las dos coordenadas.
	private boolean existsHorizontalPath(Coordinate c,
			Coordinate actualCoordinate) {

		boolean result = true;
		int begin;
		int end;

		if (c.getY() < actualCoordinate.getY()) {
			begin = c.getY() + 1;
			end = actualCoordinate.getY();
		} else {
			begin = actualCoordinate.getY() + 1;
			end = c.getY();
		}

		for (int i = begin; i < end; i++) {
			result = result && floorMap.isFreeCell(c.getX(), i);
		}

		return result;
	}

	// No hay paredes bloqueando el camino vertical entre las dos coordenadas.
	private boolean existsVerticalPath(Coordinate c, Coordinate actualCoordinate) {

		boolean result = true;
		int begin;
		int end;

		if (c.getX() < actualCoordinate.getX()) {
			begin = c.getX() + 1;
			end = actualCoordinate.getX();
		} else {
			begin = actualCoordinate.getX() + 1;
			end = c.getX();
		}

		for (int i = begin; i < end; i++) {
			result = result && floorMap.isFreeCell(i, c.getY());
		}

		return result;
	}

	// Busca la forma mas optima de cubrir verticalmente una celda importante.
	private Coordinate findOptimalVerticalCover(Coordinate c) {

		cellsCovered = 0;
		Coordinate sensorCoordinate = new Coordinate();

		for (int i = 0; i < floorMap.getN(); i++) {
			if (c.getX() != i) {
				Coordinate actualCoordinate = new Coordinate(i, c.getY());
				if (floorMap.getPos(i, c.getY()) != Floor.WALL
						&& existsVerticalPath(c, actualCoordinate)) {
					int cellsToCoverCardinal = getCellsToCoverCardinal(i,
							c.getY());
					if (cellsToCoverCardinal > cellsCovered) {
						cellsCovered = cellsToCoverCardinal;
						sensorCoordinate.setX(i);
						sensorCoordinate.setY(c.getY());
						horizontalSensorNeeded = horizontalSensorNeededTemp;
						verticalSensorNeeded = true;

					}
				}
			}
		}

		return sensorCoordinate;

	}

	// Calcula cuantas celdas ser�an cubiertas ubicando un sensor
	// en la posicion especificada.

	private int getCellsToCoverCardinal(int x, int y) {

		int freeCellsCardinal = 0;
		horizontalSensorNeededTemp = false;
		verticalSensorNeededTemp = false;

		if (trackingFloorMap.getPos(x, y) == TrackingFloor.UNCOVERED_FREE_CELL) {

			for (int i = 0; i < floorMap.getN(); i++) {
				if (trackingFloorMap.getPos(i, y) == TrackingFloor.UNCOVERED_FREE_CELL) {
					if (existsVerticalPath(new Coordinate(x, y),
							new Coordinate(i, y))) {
						freeCellsCardinal++;
						if (i != x) {
							verticalSensorNeededTemp = true;
						}
					}
				}
			}

			for (int j = 0; j < floorMap.getM(); j++) {
				if (trackingFloorMap.getPos(x, j) == TrackingFloor.UNCOVERED_FREE_CELL) {
					if (existsHorizontalPath(new Coordinate(x, y),
							new Coordinate(x, j))) {
						freeCellsCardinal++;
						if (j != y) {
							horizontalSensorNeededTemp = true;
						}
					}
				}
			}

			// Recorrimos 2 veces la posicion x,y, por eso restamos 1.
			freeCellsCardinal--;
		}

		return freeCellsCardinal;

	}

	// Una vez cubiertas las celdas importantes, se procede a cubrir el resto de
	// las celdas hasta cubrir el piso en su totalidad.
	private void coverRestOfFloor(int x, int y) {

		while (totalCellsToCover > 0) {
			for (int i = x; i < trackingFloorMap.getN(); i++) {
				for (int j = y; j < trackingFloorMap.getM(); j++) {
					if (trackingFloorMap.getPos(i, j) == TrackingFloor.UNCOVERED_FREE_CELL) {
						locateSensor(i, j);
					}

				}
			}

			if (totalCellsToCover > 0) {
				backtrackStep();
			}

		}
	}

	// Retrocede un paso en el �rbol de busqueda.
	private void backtrackStep() {

		if (!sensorsSetup.empty()) {
			// Sacamos el sensor y lo usamos para liberar las celdas que cubre.
			LocatedSensor sensor = (LocatedSensor) sensorsSetup.pop();

			// Se liberan las celdas cubiertas por el sensor eliminado.
			uncoverCells(sensor);

			// Se calcula la siguiente coordenada para continuar
			// la b�squeda.
			Coordinate sensorCoordinate = sensor.getCoordinate();

			if (sensorCoordinate.getY() + 1 < trackingFloorMap.getM()) {
				coverRestOfFloor(sensorCoordinate.getX(),
						sensorCoordinate.getY() + 1);
			} else if (sensorCoordinate.getX() + 1 < trackingFloorMap.getN()) {
				coverRestOfFloor(sensorCoordinate.getX() + 1, 0);
			} else {
				backtrackStep();
			}
		} else {
			noSolution();
		}

	}

	// Cuando no se encuentra soluci�n, se devuelve -1.
	private void noSolution() {
		System.out.println("-1");

	}

	private void uncoverCells(LocatedSensor sensor) {

		int sensorType = sensor.getSensorType();
		Coordinate c = sensor.getCoordinate();

		if (sensorType == CUAT_SENSOR) {

			for (int i = 0; i < floorMap.getN(); i++) {
				if (trackingFloorMap.getPos(i, c.getY()) != TrackingFloor.WALL_CELL
						&& existsVerticalPath(c, new Coordinate(i, c.getY()))) {
					trackingFloorMap.uncover(i, c.getY());
					if (trackingFloorMap.getPos(i, c.getY()) == TrackingFloor.UNCOVERED_FREE_CELL)
						totalCellsToCover++;
				}
			}

			for (int j = 0; j < floorMap.getM(); j++) {
				if (trackingFloorMap.getPos(c.getX(), j) != TrackingFloor.WALL_CELL
						&& existsHorizontalPath(c, new Coordinate(c.getX(), j))) {
					trackingFloorMap.uncover(c.getX(), j);
					if (trackingFloorMap.getPos(c.getX(), j) == TrackingFloor.UNCOVERED_FREE_CELL)
						totalCellsToCover++;
				}
			}

			// Cubrimos la coordenada a ubicar el sensor,
			// ya que descubrimos dos veces por recorrer las dos dimensiones.
			trackingFloorMap.cover(c.getX(), c.getY());
			minCostAccumulator -= CUAT_SENSOR_COST;

		} else if (sensorType == BI_SENSOR_VERTICAL) {
			for (int i = 0; i < floorMap.getN(); i++) {
				if (trackingFloorMap.getPos(i, c.getY()) != TrackingFloor.WALL_CELL
						&& existsVerticalPath(c, new Coordinate(i, c.getY()))) {
					trackingFloorMap.uncover(i, c.getY());
					if (trackingFloorMap.getPos(i, c.getY()) == TrackingFloor.UNCOVERED_FREE_CELL)
						totalCellsToCover++;
				}
			}
			minCostAccumulator -= BI_SENSOR_COST;

		} else if (sensorType == BI_SENSOR_HORIZONTAL) {
			for (int j = 0; j < floorMap.getM(); j++) {
				if (trackingFloorMap.getPos(c.getX(), j) != TrackingFloor.WALL_CELL
						&& existsHorizontalPath(c, new Coordinate(c.getX(), j))) {
					trackingFloorMap.uncover(c.getX(), j);
					if (trackingFloorMap.getPos(c.getX(), j) == TrackingFloor.UNCOVERED_FREE_CELL)
						totalCellsToCover++;
				}
			}
			minCostAccumulator -= BI_SENSOR_COST;
		}

	}

	// Ubica un sensor en la posici�n dada, sin tener en cuenta la cantidad de
	// celdas a influir. Se usa cuando se quiere cubrir un grupo de celdas sin
	// optimizar/maximizar
	// la cantidad de las mismas cubiertas por el sensor.
	private void locateSensor(int x, int y) {
		getCellsToCoverCardinal(x, y);

		int sensorType = -1;
		int sensorCost = BI_SENSOR_COST;

		horizontalSensorNeeded = horizontalSensorNeededTemp;
		verticalSensorNeeded = verticalSensorNeededTemp;

		if (horizontalSensorNeeded && verticalSensorNeeded) {
			sensorType = CUAT_SENSOR;
			sensorCost = CUAT_SENSOR_COST;
		} else if (horizontalSensorNeeded) {
			sensorType = BI_SENSOR_HORIZONTAL;
		} else {
			sensorType = BI_SENSOR_VERTICAL;
		}

		// Aca se realiza el backtracking.
		// Si se detecta que agregando el sensor, se excede el mejor costo
		// encontrado hasta el momento, se aborta la operaci�n de agregado del
		// sensor
		// y se retrocede un paso buscando ubicar de otra manera.

		Coordinate c = new Coordinate(x, y);
		LocatedSensor locatedSensor = new LocatedSensor(c, sensorType, false,
				false);
		sensorsSetup.push(locatedSensor);
		coverCells(c, sensorType);

	}
}
