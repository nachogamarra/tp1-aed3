package problem.problem3;

import java.util.ArrayList;
import java.util.List;

/*Este clase representa un piso del museo.
 //Se representa con una matriz*/

public class Floor {

	// Dimensiones del piso.
	private int n;
	private int m;

	// Piso.
	private int[][] floor;

	// Posiciones del piso.
	public static final int WALL = 0;
	public static final int FREE_CELL = 1;
	public static final int IMPORTANT_CELL = 2;

	// Cantidad total de celdas-pared, celdas libres, y celdas importantes.
	private int freeCells = 0;
	private int wallCells = 0;
	private int importantCells = 0;

	// Lista de celdas importantes.
	private List<Coordinate> importantCellsList = new ArrayList<Coordinate>();

	public Floor() {

	}

	public Floor(int n, int m, int[][] floor) {

		this.n = n;
		this.m = m;
		this.floor = floor;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (floor[i][j] == WALL)
					wallCells++;
				else if (floor[i][j] == IMPORTANT_CELL) {
					importantCells++;
					importantCellsList.add(new Coordinate(i, j));
				} else
					freeCells++;

			}
		}

	}

	public int getN() {
		return n;
	}

	public int getM() {
		return m;
	}

	// Copia de un floor.
	@Override
	protected Object clone() throws CloneNotSupportedException {

		Floor clonedFloor = new Floor();
		clonedFloor.n = n;
		clonedFloor.m = m;

		clonedFloor.freeCells = freeCells;
		clonedFloor.importantCells = importantCells;
		clonedFloor.wallCells = wallCells;

		clonedFloor.importantCellsList = importantCellsList;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				clonedFloor.floor[i][j] = floor[i][j];
			}
		}
		return clonedFloor;
	}

	public List<Coordinate> getImportantCellsList() {
		return importantCellsList;
	}

	public int getFreeCells() {
		return freeCells;
	}

	public int getWallCells() {
		return wallCells;
	}

	public int getImportantCells() {
		return importantCells;
	}

	public int getPos(int i, int j) {
		return floor[i][j];
	}

	// Chequea si la posicion no es una pared.
	public boolean isFreeCell(int i, int j) {
		return !isWall(i, j);
	}

	// Chequea si la posicion es una pared.
	public boolean isWall(int i, int j) {
		return (floor[i][j] == WALL);
	}
}
