package problem.problem3;

public class TrackingFloor {

	// Piso a ser cubierto con rayos de los sensores.
	// Si una celda es negativa, entonces es pared.
	// Sino, el numero indica la cantidad de sensores influyendo sobre esa
	// celda.

	private int[][] trackingFloor;

	public static final int WALL_CELL = -1;
	public static final int UNCOVERED_FREE_CELL = 0;
	public static final int JUST_COVERED_FREE_CELL = 1;

	private int n;
	private int m;

	public TrackingFloor(Floor floor) {

		trackingFloor = new int[floor.getN()][floor.getM()];

		n = floor.getN();
		m = floor.getM();

		for (int i = 0; i < floor.getN(); i++) {
			for (int j = 0; j < floor.getM(); j++) {
				if (floor.getPos(i, j) == Floor.WALL)
					trackingFloor[i][j] = WALL_CELL;
				else
					trackingFloor[i][j] = UNCOVERED_FREE_CELL;
			}
		}
	}

	public int getN() {
		return n;
	}

	public int getM() {
		return m;
	}

	// Un nuevo sensor cubre esta celda.
	void cover(int x, int y) {
		trackingFloor[x][y]++;
	}

	// Un sensor deja de cubrir esta celda.
	void uncover(int x, int y) {
		trackingFloor[x][y]--;
	}

	public int getPos(int x, int y) {

		return trackingFloor[x][y];
	}
	
	public boolean isCovered(int x, int y) {
		return trackingFloor[x][y] >= JUST_COVERED_FREE_CELL;
	}

}
