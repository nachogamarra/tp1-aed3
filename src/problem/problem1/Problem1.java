package problem.problem1;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Problem1 {

	private  float limiteCamion;
	private PriorityQueue<Camion> camiones;
	private List<Camion> camionesOrdenadosXLlegada;
	private int contadorOperaciones;
	
	// En el constructor se pasa los parametros de entrada.
	public Problem1(float limiteCamion, int cantPaquetes, float[] paquetes) {

		contadorOperaciones = 0;
		
		this.limiteCamion = limiteCamion;
		CamionComparator comparator =new CamionComparator(); 
		camiones = new PriorityQueue<Camion>(cantPaquetes, comparator);
		camionesOrdenadosXLlegada = new ArrayList<Camion>();
		
		contadorOperaciones+=4;
		

	}
	public void colocarPaquetes(float limiteCamion,int cantPaquetes, float[] paquetes) {
		// Agrega la primer carga al primer camion.
		Camion camion = new Camion(limiteCamion);
		camion.cargar(paquetes[0]);
		contadorOperaciones+=4;
		
		camiones.add(camion);
		contadorOperaciones += (int) Math.round(Math.log(camiones.size()));
		
		camionesOrdenadosXLlegada.add(camion);
		contadorOperaciones++;
		
		for (int i = 1; i < cantPaquetes; i++) {
			colocar(paquetes[i]);
			contadorOperaciones+=3;
		}
	}

	void colocar(float paquete) {
		// Tomo el camion menos cargado que esta en la raiz del Heap
		Camion camionMenosCargado = new Camion(limiteCamion);
		camionMenosCargado = camiones.peek();
		contadorOperaciones+=2;
		/*
		 * Si el paquete no supera la carga permitida entonces se carga en el
		 * camion menos cargado. Sino se carga el paquete en un nuevo camion
		 * vacio.
		 */
		contadorOperaciones+=3;
		if (camionMenosCargado.puedeCargar(paquete)) {
			camionMenosCargado.cargar(paquete);
			contadorOperaciones+=2;
			
			camiones.poll(); //Ya no necesariamente es el camion menos cargado
			camiones.add(camionMenosCargado);
			contadorOperaciones += 2 * ((int) Math.round(Math.log(camiones.size())));
			
		} else {
			Camion camion = new Camion(limiteCamion);
			camion.cargar(paquete);
			contadorOperaciones+=3;
			camiones.add(camion);
			contadorOperaciones += (int) Math.round(Math.log(camiones.size()));
			camionesOrdenadosXLlegada.add(camion);
			contadorOperaciones++;
		}
	}

	
	public int getContadorOperaciones() {
		return contadorOperaciones;
	}
	public List<Camion> getCamionesOrdenadosXLlegada(){
		return this.camionesOrdenadosXLlegada;
	}
 
}

