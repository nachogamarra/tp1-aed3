package problem.problem1;

public class Paquete {

	private float peso;

	public Paquete(float peso) {

		this.peso = peso;
	}

	public float getPeso() {
		return peso;
	}

}
