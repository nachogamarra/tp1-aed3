package problem.problem1;

public class Camion{

	
	private float capacidad;
	private float cargaActual;

	public Camion(float capacidad) {
		this.capacidad = capacidad;
		this.cargaActual = 0;
	} 

	public float getCapacidad() {
		return capacidad;
	}

	public boolean puedeCargar(float carga) {
		return (carga + cargaActual <= capacidad);
	}

	// No debe exceder la capacidad maxima.
	public void cargar(float carga) {
		this.cargaActual = cargaActual + carga;
	}

	public float getCargaActual() {
		return cargaActual;
	}


}
