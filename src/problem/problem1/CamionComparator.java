package problem.problem1;

import java.util.Comparator;

public class CamionComparator implements Comparator<Camion> {

	public int compare(Camion arg0, Camion arg1) {
		int resultado;
		if (arg0.getCargaActual() < arg1.getCargaActual())
			resultado = -1;
		else if (arg0.getCargaActual() == arg1.getCargaActual())
			resultado = 0;
		else
			resultado = 1;
		return resultado;
	}

}
