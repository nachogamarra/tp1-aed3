package output;

import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.util.List;

import problem.problem2.Curso;

public class OutputEj2 implements Output {

	private String outputFile;
	private List<List<Integer>> solutions;

	public OutputEj2(List<List<Integer>> solutions2) {
		this.solutions = solutions2;
	}

	public OutputEj2(String fileName) {

		outputFile = fileName;
	}

	@Override
	public void writeFile(String outputFile) throws Exception {
		this.outputFile = outputFile;

		BufferedWriter bufferedWriter = new BufferedWriter(new PrintWriter(
				this.outputFile));

		// Escribimos todas las soluciones.
		for (int i = 0; i < solutions.size(); i++) {

			List<Integer> solution = solutions.get(i);
			printSolution(bufferedWriter, solution);
		}

		bufferedWriter.close();

	}

	private void printSolution(BufferedWriter bufferedWriter,
			List<Integer> solution) throws Exception {

		if (!solution.isEmpty()) {

			// Primer curso.
			Integer primerCurso = solution.get(0);
			bufferedWriter.write(Integer.toString(primerCurso));

			// Resto de los cursos.
			for (int i = 1; i < solution.size(); i++) {
				bufferedWriter.write(" ");
				Integer curso = solution.get(i);
				bufferedWriter.write(Integer.toString(curso));

			}
		}

		bufferedWriter.flush();
		bufferedWriter.newLine();

	}

}
