package output;

public interface Output {

	public void writeFile(String outputFile) throws Exception;

}
