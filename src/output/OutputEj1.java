package output;

import instance.InstanceEj1;

import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.util.List;

import problem.problem1.Camion;

public class OutputEj1 implements Output {

	private String outputFile;
	private List<List<Camion>> solutions;

	public OutputEj1(List<List<Camion>> solutions) {
		this.solutions = solutions;
	}

	public OutputEj1(String fileName) {

		outputFile = fileName;
	}

	@Override
	public void writeFile(String outputFile) throws Exception {
		this.outputFile = outputFile;

		BufferedWriter bufferedWriter = new BufferedWriter(new PrintWriter(
				this.outputFile));

		// Escribimos todas las soluciones.
		for (int i = 0; i < solutions.size(); i++) {

			List<Camion> solution = solutions.get(i);
			printSolution(bufferedWriter, solution);
		}

		bufferedWriter.close();

	}

	private void printSolution(BufferedWriter bufferedWriter,
			List<Camion> solution) throws Exception {

		if (!solution.isEmpty()) {

			// Cantidad de camiones.
			int k = solution.size();
			bufferedWriter.write(String.valueOf(k));

			// Pesos de los camiones.
			for (int i = 0; i < solution.size(); i++) {
				bufferedWriter.write(" ");
				Camion camion = solution.get(i); 
				bufferedWriter.write(String.valueOf(camion.getCargaActual()));

			}
		}

		bufferedWriter.flush();
		bufferedWriter.newLine();

	}

}
