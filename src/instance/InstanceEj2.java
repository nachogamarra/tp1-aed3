package instance;

import java.util.List;

import problem.problem2.Curso;

public class InstanceEj2 extends Instance {

	private int cantidadCursos;
	private Curso[] cursos;

	public InstanceEj2(int cantidadCursos, Curso[] cursos) {

		this.cantidadCursos = cantidadCursos;
		this.cursos = cursos;
	}

	public int getCantidadCursos() {
		return cantidadCursos;
	}

	public void setCantidadCursos(int cantidadCursos) {
		this.cantidadCursos = cantidadCursos;
	}

	public Curso[] getCursos() {
		return cursos;
	}

	public void setCursos(Curso[] cursos) {
		this.cursos = cursos;
	}

}
