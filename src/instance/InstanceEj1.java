package instance;

import java.util.List;
import problem.problem1.Paquete;

public class InstanceEj1 extends Instance {

	private float limiteCamion;
	private int cantidadPaquetes;
	private float[] paquetes;

	public InstanceEj1(float limiteCamion, int cantidadCamiones,
			float[] paquetes) {

		this.limiteCamion = limiteCamion;
		this.cantidadPaquetes = cantidadCamiones;
		this.paquetes = paquetes;
	}

	public float getLimiteCamion() {
		return limiteCamion;
	}

	public void setLimiteCamion(int limiteCamion) {
		this.limiteCamion = limiteCamion;
	}

	public int getCantidadPaquetes() {
		return cantidadPaquetes;
	}

	public void setCantidadPaquetes(int cantidadPaquetes) {
		this.cantidadPaquetes = cantidadPaquetes;
	}

	public float[] getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(float[] paquetes) {
		this.paquetes = paquetes;
	}

}
