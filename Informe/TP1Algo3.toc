\select@language {spanish}
\contentsline {section}{\numberline {1}Problema 1: Pascual y el correo}{3}
\contentsline {subsection}{\numberline {1.1}Introducci\'on}{3}
\contentsline {subsection}{\numberline {1.2}Algoritmo}{3}
\contentsline {subsection}{\numberline {1.3}Desarrollo}{4}
\contentsline {subsection}{\numberline {1.4}Correctitud}{4}
\contentsline {subsection}{\numberline {1.5}Complejidad}{5}
\contentsline {subsection}{\numberline {1.6}Testing}{5}
\contentsline {subsection}{\numberline {1.7}Conclusiones}{5}
\contentsline {section}{\numberline {2}Problema 2: Profesores Visitantes}{6}
\contentsline {subsection}{\numberline {2.1}Introducci\'on}{6}
\contentsline {subsection}{\numberline {2.2}Algoritmo}{6}
\contentsline {section}{\numberline {3}Problema 3: Una Noche en el Museo}{8}
