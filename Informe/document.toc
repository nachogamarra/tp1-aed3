\select@language {spanish}
\contentsline {section}{\numberline {1}Problema 1}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Introducci\'on}{3}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Ejemplo 1: Instancia arbitraria}{3}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Ejemplo 2: Instancia en la que se utiliza 1 solo cami\'on:}{4}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}Instancia donde se utilizan igual cantidad camiones que de paquetes:}{4}{subsubsection.1.1.3}
\contentsline {subsection}{\numberline {1.2}Desarrollo}{4}{subsection.1.2}
\contentsline {paragraph}{}{4}{section*.2}
\contentsline {paragraph}{}{4}{section*.3}
\contentsline {paragraph}{}{4}{section*.4}
\contentsline {subsection}{\numberline {1.3}Pseudoc\'odigo}{4}{subsection.1.3}
\contentsline {paragraph}{}{5}{section*.5}
\contentsline {subsection}{\numberline {1.4}Correctitud}{5}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Complejidad}{6}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Tests}{6}{subsection.1.6}
\contentsline {paragraph}{}{6}{section*.6}
\contentsline {subsubsection}{\numberline {1.6.1}Instancia arbitraria: }{6}{subsubsection.1.6.1}
\contentsline {subsubsection}{\numberline {1.6.2}Instancia que utiliza 1 solo cami\'on: }{7}{subsubsection.1.6.2}
\contentsline {subsubsection}{\numberline {1.6.3}Instancia que necesita $n$ camiones: }{7}{subsubsection.1.6.3}
\contentsline {subsubsection}{\numberline {1.6.4}Cantidad estimada de instrucciones}{7}{subsubsection.1.6.4}
\contentsline {paragraph}{}{7}{section*.7}
\contentsline {subsection}{\numberline {1.7}Conclusiones}{7}{subsection.1.7}
\contentsline {section}{\numberline {2}Problema 2}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}Introducci\'on}{8}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Desarrollo}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Pseudoc\'odigo}{8}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Correctitud}{9}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Complejidad}{10}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Conclusiones}{10}{subsection.2.6}
\contentsline {section}{\numberline {3}Problema 3}{11}{section.3}
\contentsline {subsection}{\numberline {3.1}Introducci\'on}{11}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Desarrollo}{11}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Pseudoc\'odigo}{11}{subsection.3.3}
\contentsline {section}{\numberline {4}Informe de Modificaciones}{13}{section.4}
\contentsline {subsection}{\numberline {4.1}Problema 1}{13}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Introducci\'on}{13}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Desarrollo}{13}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Pseudoc\'odigo}{13}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Complejidad}{13}{subsubsection.4.1.4}
\contentsline {subsubsection}{\numberline {4.1.5}Tests}{13}{subsubsection.4.1.5}
