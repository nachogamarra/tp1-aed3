\section{Problema 3}

\subsection{Introducci\'on}

En el presente problema, se necesita instalar un sistema de seguridad en el piso de 
un museo. El mismo se representa como un matriz o grilla de \textit{n x m}. 
Cada posición de la grilla puede contener una pared o un espacio libre, y se debe 
cubrir toda el área del piso instalando sensores láser. Se dispone de dos tipos de sensores: 
cuatridireccionales y bidireccionales (horizontales o verticales), con sus respectivos costos. 
Se debe tener en cuenta que los sensores no pueden apuntarse entre sí, y que algunas de las 
posiciones libres son 'especiales', es decir, deben ser cubiertas por dos sensores, tanto 
horizontal como verticalmente. \\

El objetivo del algoritmo a implementar es obtener una disposición de 
sensores tal que cubran toda el área libre con el menor costo posible, 
teniendo en cuenta las posiciones especiales.
Si no es posible encontrar una solución, se debe indicar esta situación.

\subsection{Desarrollo}

Encontramos cierta similitud entre este problema y el de las \textit{n} Reinas, donde las reinas
deben ubicarse en un tablero de \textit{n x n}, sin amenazarse entre ellas.\footnote{http://es.wikipedia.org/wiki/Problema\_de\_las\_ocho\_reinas.} 
\\
Se decidió implementar la solución de este problema usando backtracking, debido a 
que permite ir considerando las distintas permutaciones/combinaciones de configuración 
de sensores como distintas soluciones posibles dentro de un árbol de soluciones. 
Efectuando ciertas comprobaciones y verificaciones, se espera reducir el tiempo de 
ejecución de este procedimiento. \\
La idea es ir ubicando sensores y cubriendo el área, tratando de maximizar 
las casillas libres que se cubren con cada sensor que se instala, teniendo en cuenta
las restricciones del problema. Dichas restricciones son que los sensores no se pueden
apuntar entre sí, y que las casillas libres importantes/especiales deben ser cubiertas
por dos sensores de manera horizontal y vertical.\\
A medida que se va generando la solución ubicando sensores, tambien se va precalculando
el costo total de los sensores.

El algoritmo resuelve este problema en dos etapas: 
\begin{enumerate}
\item El algoritmo ubica sensores de manera que todas las casillas importantes sean cubiertas, tal como es requerido.
\item Una vez cubiertas todas las casillas importantes, se procede a cubrir el resto de casillas libres.
\end{enumerate}

\subsubsection{Descripción}
El algoritmo usa una estructura de datos pila (Stack), para ir ubicando/recolectando 
los sensores que conforman la solución del problema. La idea es que cuando se llega a una 
inconsistencia o imposibilidad para seguir agregando sensores, se retroceda en el árbol 
de posibles soluciones. De esta manera, se puede obtener el tope de la pila, como el último 
sensor agregado, e intentar buscar otro camino para encontrar la solución, efectuando así la 
vuelta atrás(backtraking). \\
Por otro lado, se mantiene un mapa del piso en todo momento de la ejecución del algoritmo, 
con el estado de cuantos sensores apuntan a cada posición libre del piso.\footnote{Dicha funcionalidad 
es provista por la clase \textit{TrackingFloor.java.}} Por ejemplo, si en este mapa
tenemos una determinada posición con el número 0, esto indica que esa casilla libre aún no ha sido cubierta.
Si dicha casilla tiene un número $n > 0$, esto indica que hay n sensores apuntando a esa casilla. 



\subsubsection{Primera Etapa}
Esta etapa intenta cubrir todas las celdas importantes de manera óptima.
Por cada celda importante, el algoritmo busca ubicar sensores que la cubran tanto 
de manera horizontal como vertical. Se eligen posiciones tal que maximizen la cantidad de
casillas libres cubiertas, tratando de cubrir lo más rápido posible el área total.
Si en algún tramo de la operación, se encuentra que no se puede seguir cubriendo el total
de casillas importantes, se realiza el retroceso buscando otra disposición de los sensores, 
ya sea de posición o de calidad (cuatridireccional  o bidireccional).\footnote{Este procesamiento 
es realizado por la funcion \textit{coverImportantCell}.}\\

\subsubsection{Segunda Etapa}
Esta última etapa, se encarga de cubrir el resto de casillas libres restantes, hasta completar la 
instalación de los sensores en todo el piso.\footnote{función \textit{coverRestOfFloor}.}\\


\subsubsection{Poda}
Consideramos que nuestra política de ubicación de sensores para cubrir casillas importantes, 
presente en la primer etapa de procesamiento, es una poda válida de nuestro algoritmo. Debido 
a que se intenta ubicar los sensores de manera óptima para cubrir la mayor cantidad de casillas,
estamos evitando que se realice una búsqueda mucho mas exhaustiva. \\


\subsection{Pseudoc\'odigo}


\subsection{Correctitud}
Si existe una solución para el problema, el algoritmo la encontrará debido a que se verifican 
las siguientes condiciones: \\

\begin{itemize}
\item las casillas importantes son cubiertas de la mejor manera posible, procurando cubrir la mayor cantidad de casillas vacias.
\item siempre que se pueda ubicar un sensor cuatridireccional se lo ubica, intentando cubrir mas casillas libres.
\item si no es necesario un sensor cuatridireccional, se ubica uno bidimensional ahorrando costo.
\item los sensores no se apuntan entre sí.
\end{itemize}


\subsection{Complejidad}

Cálculo de los tiempos de ejecución de las funciones: \\ 
Usamos $T(f)$, para denotar el tiempo de ejecución calculado para la función $f$. Aquellas operaciones cuya complejidad 
no es especificada, se asume que su complejidad es $O(1)$. \\


$T(coverFloor) = T(coverImportantCells) + T(coverRestOfFloor)$

$T(coverImportantCells) = \#celdas importantes * T(coverImportantCell) <= n*m*T(coverImportantCell)$ (peor caso)

$T(existsVerticalPath) = O(n).$

$T(existsHorizontalPath) = O(m).$

$T(isVerticallyCovered) = \#maxima de sensores * O(n).$

$T(isHorizontallyCovered) = \#maxima de sensores * O(m).$

$T(findOptimalVerticalCover) = O(n^2)* O (n^2 + m^2) = O (n^4+ (nm)^2)$

$T(findOptimalHorizontalCover) = O(m^2)* O (n^2 + m^2) = O (m^4+ (nm)^2)$

$T(getCellsToCoverCardinal) = O (n^2 + m^2) $ (peor caso)

$T(coverCells) = O (n^2 + m^2) $ (peor caso)

$T(uncoverCells) = O (n^2 + m^2)$  (peor caso)
\\

Cuando se encuentra alguna posición para ubicar los sensores para cubrir una casilla importante, tenemos: \\

$T(coverImportantCell) =   2*T(coverCells) + T(isVerticallyCovered) + T(isHorizontallyCovered)$ \\ 
		 $+ T(findOptimalVerticalCover) + T(findOptimalHorizontalCover)$
\\

Cuando no se encuentra posición para ubicar un sensor, se llama a la funcion
\textit{backtrackStepImportantCells}, la cual efectúa el retroceso, y desapila un sensor 
de la pila: \\

$T(coverImportantCell) = T(isVerticallyCovered) + T(backtrackStepImportantCells)$
\\

\textit{backtrackStepImportantCells} llama a \textit{coverImportantCell}, con lo cual tenemos una recursion ímplicita entre \textit{coverImportantCell} 
y \textit{backtrackStepImportantCells}. \\

$T(backtrackStepImportantCells) = T(coverCells) + T(uncoverCells) = O (n^2 + m^2) + T(coverImportantCell)$ \\

En cada llamada a la recursión, sabemos que se está desapilando un sensor de la pila. Entonces sabemos que 
la complejidad de \textit{coverImportantCell} depende de la cantidad de sensores en la pila en ese momento. Como no es posible
acotar con exactitud la cantidad de sensores que tendrá la pila en determinado momento, ya que depende de la forma o topología del piso,
debemos aplicar una cota gruesa suponiendo que podemos tener como máximo un sensor en cada posición libre.
Esto nos da como máximo $n * m$ casillas libres, entonces esta cota gruesa nos hace determinar que la complejidad de \textit{coverImportantCell} es \textbf{O((n*m)!)} \\

El análisis de complejidad de la segunda etapa del algoritmo: $T(CoverRestOfFloor)$, es bastante similar al 
análisis de la primer parte. \\

Por lo tanto, la complejidad temporal de textit{coverImportantCell} fuerza a la complejidad temporal del algoritmo total a ser 
\textbf{O((n*m)!}) \\

\subsection{Tests}


\subsection{Conclusiones}


\subsection{Código}

\color{blue} Agregar codigo aca

\color{black}




